#include <QSettings>
#include <QProcess>
#include <QDebug>
#include <QDateTime>
#include "J2534Device.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_gui(new Ui::MainWindow)
{
    m_gui->setupUi(this);
    connect(&m_j2534, SIGNAL(deviceError(QString)), this, SLOT(onDeviceError(QString)));
    connect(&m_j2534, SIGNAL(isOpenChanged(bool)), this, SLOT(isOpenChanged(bool)));
    connect(&m_channel, SIGNAL(readyRead()), this, SLOT(onNewData()));

    foreach (QString device, J2534Device::devices())
    {
        QString name = J2534Device::deviceVendor(device) + " / " + J2534Device::deviceName(device);
        m_gui->comboBox->addItem(name, device);
    }
}

MainWindow::~MainWindow()
{
    delete m_gui;
}

void MainWindow::parseMessage(quint32 id, const char *data, int size)
{
    QString dump;
    for (int i = 0; i < size; i++)
    {
        QString num = QString::number((quint8)data[i], 16);
        if (num.size() == 1) num.prepend("0");
        if (dump.size() > 0) dump.append(" ");
        dump.append(num);
    }

    int rowIndex = m_gui->tableWidget->rowCount();
    m_gui->tableWidget->setRowCount(rowIndex + 1);
    QTableWidgetItem *itemTime = new QTableWidgetItem(QDateTime::currentDateTime().toString("hh:mm:ss.zzz"));
    QTableWidgetItem *itemId = new QTableWidgetItem(QString("0x") + QString::number(id, 16));
    QTableWidgetItem *itemDLC = new QTableWidgetItem(QString::number(size));
    QTableWidgetItem *itemData = new QTableWidgetItem(dump);
    m_gui->tableWidget->setItem(rowIndex, 0, itemTime);
    m_gui->tableWidget->setItem(rowIndex, 1, itemId);
    m_gui->tableWidget->setItem(rowIndex, 2, itemDLC);
    m_gui->tableWidget->setItem(rowIndex, 3, itemData);
}

void MainWindow::onDeviceError(QString error)
{
    qDebug() << "MainWindow onError" << error;
}

void MainWindow::isOpenChanged(bool isOpen)
{
    qDebug() << "MainWindow isOpenChanged" << isOpen;
}

void MainWindow::onNewData()
{
    while (m_channel.packetsAvailable())
    {
//        QByteArray data = m_channel.readData();
//        parseMessage(0, data.constData(), data.size());
        quint32 canId;
        char data[4096];
        quint32 size = m_channel.readPacket(&canId, data, sizeof(data));
        parseMessage(canId, data, size);
    }
}

void MainWindow::on_pushButtonConfigure_clicked()
{
    QString device = m_gui->comboBox->itemData(m_gui->comboBox->currentIndex()).toString();
    QString configTool = J2534Device::deviceConfigApplication(device);
    if (configTool.size() != 0)
    {
        QStringList arguments;
        arguments << configTool;
        QProcess::execute("C:\\windows\\explorer.exe", arguments);
    }
}

void MainWindow::on_pushButtonLoadDll_clicked()
{
    QString device = m_gui->comboBox->itemData(m_gui->comboBox->currentIndex()).toString();
    QString functionLibrary = J2534Device::deviceFunctionLibrary(device);
    bool retval = m_j2534.setLibrary(functionLibrary);
    qDebug() << "MainWindow" << (retval ? "OK" : m_j2534.errorString());
}

void MainWindow::on_pushButtonUnloadDll_clicked()
{
    bool retval = m_j2534.setLibrary("");
    qDebug() << "MainWindow" << (retval ? "OK" : m_j2534.errorString());
}

void MainWindow::on_pushButtonOpen_clicked()
{
    bool retval = m_j2534.open();
    qDebug() << "MainWindow" << (retval ? "OK" : m_j2534.errorString());
}

void MainWindow::on_pushButtonClose_clicked()
{
    m_j2534.close();
}

void MainWindow::on_pushButtonConnect_clicked()
{
    bool retval = m_channel.open(&m_j2534);
    qDebug() << "MainWindow" << (retval ? "OK" : m_channel.errorString());

    if (retval)
    {
        bool retval = m_channel.setAcceptanceFilter(0, 0);
        qDebug() << "MainWindow passFilter" << (retval ? "OK" : m_channel.errorString());
    }
}

void MainWindow::on_pushButtonDisconnect_clicked()
{
    m_channel.close();
}

