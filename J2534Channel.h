#ifndef J2534CHANNEL_H
#define J2534CHANNEL_H

#include <QObject>
#include <QString>
#include <QByteArray>
#include <QList>
class J2534Device;

class J2534Channel : public QObject
{
    Q_OBJECT
public:
    explicit J2534Channel(QObject *parent = 0);
    ~J2534Channel();

    enum CanProtocol
    {
        RawCAN,
        ISO15765
    };

    /**
     * \brief Connects the channel
     *
     * This call is blocking and will wait for the channel to be established.
     *
     * \param[in] device The J2534 device to connect through
     * \param[in] baudrate Requested bitrate for this channel
     * \param[in] protocol Raw CAN or ISO15765
     *
     * \retval true Channel successfully connected
     * \retval false Failed to connect, check errorString() for details.
     */
    virtual bool open(J2534Device *device, CanProtocol protocol = RawCAN, quint32 baudrate = 250000);

    /**
     * \brief Disconnects this channel
     *
     * Unread packets will remain in the buffer, but no futher packets
     * will be received.
     */
    virtual void close();

    /**
     * \brief Check if the channel is open
     *
     * \retval true Channel is open
     * \retval false Channel is closed
     */
    virtual bool isOpen() const;

    /**
     * \brief Get number of available packets
     *
     * \returns Number of available packets in buffer, or 0 if there are none.
     */
    quint32 packetsAvailable() const;

    /**
     * \brief Setup packet acceptance filter
     *
     * \param[in] maskId CAN ID's of received will be AND:ed with this ID
     * \param[in] patternId The AND:ed result will be compared to this pattern
     *                      if it matches, the packet is accepted.
     *
     * \retval true Filter was successfully set
     * \retval false Failed to set filter, check errorString() for details.
     *
     * \note By default, all packets are blocked. An acceptance filter
     *       always have to be set.
     */
    bool setAcceptanceFilter(quint32 maskId, quint32 patternId);

    /**
     * \brief Get the payload size of the next packet
     *
     * You cannot use this function to determine if there are more packets
     * to read or not. A return value of 0 could mean that either there are
     * no packets available, or that the next packet has no payload data.
     *
     * \returns Payload size of next packet
     */
    quint32 packetSize() const;

    /**
     * \brief Read a packet from the buffer
     *
     * This function is non-blocking, it will return instantly if there
     * are no packets available.
     *
     * \param[out] canId The received CAN ID will be stored here
     * \param[out] data Buffer to receive the data
     * \param[in] maxSize The maximum size of the buffer
     *
     * \returns Number of bytes in data, or 0 if no packet was available.
     */
    quint32 readPacket(quint32* canId, char* data, quint32 maxSize);

    /**
     * \brief Write a packet to the outgoing buffer
     *
     * This function is non-blocking, it will just add the packet
     * to the outgoing buffer. There is no indication if the packet
     * was actually sent or not.
     *
     * \param[in] canId The CAN ID to send
     * \param[in] data The payload data
     * \param[in] size Size of the payload data.
     */
    void writePacket(quint32 canId, const char *data, quint32 size);

    /**
     * \brief Read packet data
     *
     * This is an alternative function to readPacket, it will only
     * return the payload data of the packet and not it's CAN ID.
     *
     * This function is non-blocking, it will return instantly if there
     * are no packets available.
     *
     * \returns Payload data, or empty QByteArray if no packet was
     *          available.
     */
    QByteArray readData();

    /**
     * \brief Write packet using default CAN ID
     *
     * This is an alternative function to writePacket, it will use
     * the default CAN ID when sending packets.
     *
     * This function is non-blocking, it will just add the packet
     * to the outgoing buffer. There is no indication if the packet
     * was actually sent or not.
     *
     * \param[in] data The payload data to send.
     */
    void writeData(QByteArray data);

    /**
     * \brief Get the last error message
     *
     * \returns Human-readable string describing the last error that occured.
     */
    QString errorString() const;

signals:
    /**
     * \brief Indication that new packets are available
     *
     * This signal indicates that one or more packets are available
     * for reading. If many packets are received at once, only one
     * readyRead() will be emitted.
     *
     * \note The slot connected to this needs to read out ALL available packets.
     */
    void readyRead();

// Interface for J2534 device
protected:
    void setErrorString(QString error);
    void setChannelId(unsigned long id);
    unsigned long channelId();
    void setDeviceClosed();
    void addMessages(void *dataObjects, int objectCount);
    friend class J2534Device;

private:
    class Chunk;
    QList<Chunk*> m_chunks;
    unsigned long m_channelId;
    J2534Device *m_device;
    bool m_isOpen;
    QString m_errorString;
    quint32 m_availablePackets;
};

#endif // J2534CONNECTION_H
