#include <QDebug>
#include <QSettings>
#include <QLibrary>
#include <QThread>
#include <QApplication>
#include <QGenericArgument>
#include <QTimer>
#include "J2534Device.h"
#include "J2534Channel.h"

static const QString l_passthruPath = "HKEY_LOCAL_MACHINE\\Software\\PassThruSupport.04.04";

// Return codes for all PassThru functions
#define STATUS_NOERROR             0x00
#define ERR_NOT_SUPPORTED          0x01
#define ERR_INVALID_CHANNEL_ID     0x02
#define ERR_INVALID_PROTOCOL_ID    0x03
#define ERR_NULL_PARAMETER         0x04
#define ERR_INVALID_IOCTL_VALUE    0x05
#define ERR_INVALID_FLAGS          0x06
#define ERR_FAILED                 0x07
#define ERR_DEVICE_NOT_CONNECTED   0x08
#define ERR_TIMEOUT                0x09
#define ERR_INVALID_MSG            0x0A
#define ERR_INVALID_TIME_INTERVAL  0x0B
#define ERR_EXCEEDED_LIMIT         0x0C
#define ERR_INVALID_MSG_ID         0x0D
#define ERR_DEVICE_IN_USE          0x0E
#define ERR_INVALID_IOCTL_ID       0x0F
#define ERR_BUFFER_EMPTY           0x10
#define ERR_BUFFER_FULL            0x11
#define ERR_BUFFER_OVERFLOW        0x12
#define ERR_PIN_INVALID            0x13
#define ERR_CHANNEL_IN_USE         0x14
#define ERR_MSG_PROTOCOL_ID        0x15
#define ERR_INVALID_FILTER_ID      0x16
#define ERR_NO_FLOW_CONTROL        0x17
#define ERR_NOT_UNIQUE             0x18
#define ERR_INVALID_BAUDRATE       0x19
#define ERR_INVALID_DEVICE_ID      0x1A

// Filter types for PassThruStartMsgFilter
#define PASS_FILTER                0x00000001UL
#define BLOCK_FILTER               0x00000002UL
#define FLOW_CONTROL_FILTER        0x00000003UL

// Protocol types
#define PROTOCOL_CAN               0x05
#define PROTOCOL_ISO15765          0x06

// Bitmask flags for connection and filters
#define CAN_29BIT_ID               0x100

typedef struct {
    unsigned long ProtocolID;
    unsigned long RxStatus;
    unsigned long TxFlags;
    unsigned long Timestamp;
    unsigned long DataSize;
    unsigned long ExtraDataIndex;
    unsigned char Data[4128];
} PASSTHRU_MSG;

// This thread is dedicated to making J2534 API calls
// All calls to J2534 API _MUST_ be from this thread _ONLY_
class J2534Thread : public QThread
{
    Q_OBJECT
private:
    // J2534 DLL functions
    typedef long WINAPI (*PassThruOpenFunction)(void*, unsigned long*);
    typedef long WINAPI (*PassThruCloseFunction)(unsigned long);
    typedef long WINAPI (*PassThruConnectFunction)(unsigned long, unsigned long, unsigned long, unsigned long, unsigned long *);
    typedef long WINAPI (*PassThruDisconnectFunction)(unsigned long);
    typedef long WINAPI (*PassThruReadMsgsFunction)(unsigned long, PASSTHRU_MSG*, unsigned long*, unsigned long);
    typedef long WINAPI (*PassThruWriteMsgsFunction)(unsigned long, PASSTHRU_MSG*, unsigned long*, unsigned long);
    typedef long WINAPI (*PassThruStartPeriodicMsgFunction)(unsigned long, PASSTHRU_MSG*, unsigned long*, unsigned long);
    typedef long WINAPI (*PassThruStopPeriodicMsgFunction)(unsigned long, unsigned long);
    typedef long WINAPI (*PassThruStartMsgFilterFunction)(unsigned long, unsigned long, PASSTHRU_MSG*, PASSTHRU_MSG*, PASSTHRU_MSG*, unsigned long*);
    typedef long WINAPI (*PassThruStopMsgFilterFunction)(unsigned long, unsigned long);
    typedef long WINAPI (*PassThruReadVersionFunction)(unsigned long, char *, char *, char *);
    typedef long WINAPI (*PassThruGetLastErrorFunction)(char *);
    PassThruOpenFunction m_passThruOpen;
    PassThruCloseFunction m_passThruClose;
    PassThruConnectFunction m_passThruConnect;
    PassThruDisconnectFunction m_passThruDisconnect;
    PassThruReadMsgsFunction m_passThruReadMsgs;
    PassThruWriteMsgsFunction m_passThruWriteMsgs;
    PassThruStartPeriodicMsgFunction m_passThruStartPeriodicMsg;
    PassThruStopPeriodicMsgFunction m_passThruStopPeriodicMsg;
    PassThruStartMsgFilterFunction m_passThruStartMsgFilter;
    PassThruStopMsgFilterFunction m_passThruStopMsgFilter;
    PassThruReadVersionFunction m_passThruReadVersion;
    PassThruGetLastErrorFunction m_passThruGetLastError;

public:
    J2534Thread(QObject *parent = 0) : QThread(parent),
        m_timer(this),
        m_library(this)
    {
        // Parent of member objects must be set so that they are
        // also affected by the moveToThread() call.
        clearPassThruFunctions();
        m_isOpen = false;
    }

    ~J2534Thread()
    {
        this->terminate();
    }

    void run()
    {
        qDebug() << QThread::currentThreadId() << "T:run";
        connect(&m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
        m_timer.setSingleShot(false);
        m_timer.start(1);
        exec();
    }


// Outgoing messages from the thread
signals:
    void error(QString message);
    void channelData(unsigned long channelId, void *dataObjects, int objectCount);

// Incoming messages to the thread, should be call with BlockingQueuedConnection to get return value
public slots:
    QString loadLibrary(QString functionLibraryPath)
    {
        qDebug() << QThread::currentThreadId() << "T:loadLibrary" << functionLibraryPath;

        if (m_library.isLoaded())
        {
            QString errorString = unloadLibrary();
            if (errorString.size() != 0)
            {
                return errorString;
            }
        }

        m_library.setFileName(functionLibraryPath);
        bool loaded = m_library.load();

        if (!loaded)
        {
            return QString("Failed to load function library DLL");
        }

        m_passThruOpen = (PassThruOpenFunction)m_library.resolve("PassThruOpen");
        m_passThruClose = (PassThruCloseFunction)m_library.resolve("PassThruClose");
        m_passThruConnect = (PassThruConnectFunction)m_library.resolve("PassThruConnect");
        m_passThruDisconnect = (PassThruDisconnectFunction)m_library.resolve("PassThruDisconnect");
        m_passThruReadMsgs = (PassThruReadMsgsFunction)m_library.resolve("PassThruReadMsgs");
        m_passThruWriteMsgs = (PassThruWriteMsgsFunction)m_library.resolve("PassThruWriteMsgs");
        m_passThruStartPeriodicMsg = (PassThruStartPeriodicMsgFunction)m_library.resolve("PassThruStartPeriodicMsg");
        m_passThruStopPeriodicMsg = (PassThruStopPeriodicMsgFunction)m_library.resolve("PassThruStopPeriodicMsg");
        m_passThruStartMsgFilter = (PassThruStartMsgFilterFunction)m_library.resolve("PassThruStartMsgFilter");
        m_passThruStopMsgFilter = (PassThruStopMsgFilterFunction)m_library.resolve("PassThruStopMsgFilter");
        m_passThruReadVersion = (PassThruReadVersionFunction)m_library.resolve("PassThruReadVersion");
        m_passThruGetLastError = (PassThruGetLastErrorFunction)m_library.resolve("PassThruGetLastError");

        if (m_passThruOpen == 0 ||
                m_passThruClose == 0 ||
                m_passThruConnect == 0 ||
                m_passThruDisconnect == 0 ||
                m_passThruReadMsgs == 0 ||
                m_passThruWriteMsgs == 0 ||
                m_passThruStartPeriodicMsg == 0 ||
                m_passThruStopPeriodicMsg == 0 ||
                m_passThruStartMsgFilter == 0 ||
                m_passThruStopMsgFilter == 0 ||
                m_passThruReadVersion == 0 ||
                m_passThruGetLastError == 0)
        {
            unloadLibrary();
            clearPassThruFunctions();
            return QString("Failed to resolve the required library functions");
        }

        return QString(); // OK
    }

    QString unloadLibrary()
    {
        qDebug() << QThread::currentThreadId() << "T:unloadLibrary";

        m_isOpen = false;

        if (m_library.isLoaded())
        {
            bool retval = m_library.unload();
            if (retval == false)
            {
                return QString("Library cannot be unloaded");
            }
            else
            {
                clearPassThruFunctions();
                return QString(); // OK
            }
        }
        else
        {
            return QString(); // OK, not loaded
        }
    }

    QString open()
    {
        qDebug() << QThread::currentThreadId() << "T:open";

        if (m_passThruOpen == 0)
        {
            return QString("PassThruOpen, function has not been resolved");
        }

        m_isOpen = false;
        long retval = m_passThruOpen(0, &m_deviceId);

        switch (retval)
        {
        case STATUS_NOERROR:
            return QString(); // OK
        case ERR_DEVICE_NOT_CONNECTED:
            return QString("PassThruOpen, unable to communicate with the device");
        case ERR_DEVICE_IN_USE:
            return QString("PassThruOpen, device is currently open");
        case ERR_NULL_PARAMETER:
            return QString("PassThruOpen, NULL pointer supplied where a valid pointer is required");
        case ERR_FAILED:
            return QString("PassThruOpen, ") + passThruError();
        default:
            return QString("PassThruOpen, unknown error or software bug");
        }
    }

    QString close()
    {
        qDebug() << QThread::currentThreadId() << "T:close";

        // TODO: Notify all connections that we are disconnected

        if (m_passThruClose == 0)
        {
            return QString("PassThruClose, function has not been resolved");
        }

        m_isOpen = false;
        long retval = m_passThruClose(m_deviceId);

        switch (retval)
        {
        case STATUS_NOERROR:
            return QString(); // OK
        case ERR_DEVICE_NOT_CONNECTED:
            return QString("PassThruClose, unable to communicate with device");
        case ERR_INVALID_DEVICE_ID:
            return QString("PassThruClose, device ID invalid");
        case ERR_FAILED:
            return QString("PassThruClose, ") + passThruError();
        default:
            return QString("PassThruOpen, unknown error or software bug");
        }
    }

    QString connectChannel(unsigned long *pChannelId, unsigned long protocolId, unsigned long flags, unsigned long baudrate)
    {
        qDebug() << QThread::currentThreadId() << "T:connectChannel" << protocolId << flags << baudrate;
        if (m_passThruConnect == 0)
        {
            return QString("PassThruConnect, function has not been resolved");
        }

        long retval = m_passThruConnect(m_deviceId, protocolId, flags, baudrate, pChannelId);

        switch (retval)
        {
        case STATUS_NOERROR:
            if (m_channelIds.contains(*pChannelId) == false)
            {
                m_channelIds.append(*pChannelId);
            }
            return QString(); // OK
        case ERR_DEVICE_NOT_CONNECTED:
            return QString("PassThruConnect, unable to communicate with device");
        case ERR_NOT_SUPPORTED:
            return QString("PassThruConnect, unsupported protocol or flags");
        case ERR_INVALID_DEVICE_ID:
            return QString("PassThruConnect, device ID invalid");
        case ERR_INVALID_PROTOCOL_ID:
            return QString("PassThruConnect, invalid ProtocolID value or unsupported protocol");
        case ERR_NULL_PARAMETER:
            return QString("PassThruConnect, NULL pointer supplied where a valid pointer is required");
        case ERR_INVALID_FLAGS:
            return QString("PassThruConnect, invalid flag values");
        case ERR_INVALID_BAUDRATE:
            return QString("PassThruConnect, invalid baudrate");
        case ERR_FAILED:
            return QString("PassThruConnect, ") + passThruError();
        case ERR_CHANNEL_IN_USE:
            return QString("PassThruConnect, channel number is currently connected");
        default:
            return QString("PassThruConnect, unknown error or software bug");
        }
    }

    QString disconnectChannel(unsigned long channelId)
    {
        qDebug() << QThread::currentThreadId() << "T:disconnectChannel" << channelId;
        if (m_passThruDisconnect == 0)
        {
            return QString("PassThruDisconnect, function has not been resolved");
        }

        m_channelIds.removeOne(channelId);

        long retval = m_passThruDisconnect(channelId);

        switch (retval)
        {
        case STATUS_NOERROR:
            return QString(); // OK
        case ERR_DEVICE_NOT_CONNECTED:
            return QString("PassThruDisconnect, unable to communicate with device");
        case ERR_INVALID_DEVICE_ID:
            return QString("PassThruDisconnect, device ID invalid");
        case ERR_FAILED:
            return QString("PassThruDisconnect, ") + passThruError();
        case ERR_INVALID_CHANNEL_ID:
            return QString("PassThruDisconnect, invalid ChannelID value");
        default:
            return QString("PassThruConnect, unknown error or software bug");
        }
    }

    QString channelFilterSetup(unsigned long channelId, unsigned long filterType, PASSTHRU_MSG *maskMsg,
                               PASSTHRU_MSG *patternMsg, PASSTHRU_MSG *flowControlMsg, unsigned long *filterId)
    {
        qDebug() << QThread::currentThreadId() << "T:channelFilterSetup" << channelId;
        if (m_passThruStartMsgFilter == 0)
        {
            return QString("PassThruStartMsgFilter, function has not been resolved");
        }

        long retval = m_passThruStartMsgFilter(channelId, filterType, maskMsg, patternMsg, flowControlMsg, filterId);

        switch (retval)
        {
        case STATUS_NOERROR:
            return QString(); // OK
        case ERR_DEVICE_NOT_CONNECTED:
            return QString("PassThruStartMsgFilter, unable to communicate with device");
        case ERR_INVALID_DEVICE_ID:
            return QString("PassThruStartMsgFilter, device ID invalid");
        case ERR_INVALID_CHANNEL_ID:
            return QString("PassThruStartMsgFilter, invalid ChannelID value");
        case ERR_INVALID_MSG:
            return QString("PassThruStartMsgFilter, invalid message structure");
        case ERR_NULL_PARAMETER:
            return QString("PassThruStartMsgFilter, NULL pointer supplied where a valid pointer is required");
        case ERR_FAILED:
            return QString("PassThruStartMsgFilter, ") + passThruError();
        case ERR_NOT_UNIQUE:
            return QString("PassThruStartMsgFilter, a CAN ID matches an existing filter");
        case ERR_EXCEEDED_LIMIT:
            return QString("PassThruStartMsgFilter, exceeded the maximum number of filter");
        case ERR_MSG_PROTOCOL_ID:
            return QString("PassThruStartMsgFilter, protocol mismatch");
        default:
            return QString("PassThruStartMsgFilter, unknown error or software bug");
        }
    }

private slots:

    void onTimeout()
    {
        if (m_passThruReadMsgs != 0)
        {
            foreach (unsigned long channelId, m_channelIds)
            {
//                qDebug() << QThread::currentThreadId() << "T:onTimeout" << channelId;

                PASSTHRU_MSG msg[32];
                unsigned long numMsgs = 32;
                unsigned long timeout = 0;
                long retval = m_passThruReadMsgs(channelId, msg, &numMsgs, timeout);

                if (retval == STATUS_NOERROR)
                {
                    if (numMsgs > 0)
                    {
                        // TODO: This copy can be avoided, buffer can be malloc:ed before
                        // PassThruReadMsgs is called, and reused if there were no messages.

                        PASSTHRU_MSG *dataObjects = (PASSTHRU_MSG *)malloc(sizeof(PASSTHRU_MSG) * numMsgs);
                        for (unsigned long i = 0; i < numMsgs; i++)
                        {
                            memcpy(&dataObjects[i], &msg[i], sizeof(PASSTHRU_MSG));
                        }
                        emit channelData(channelId, dataObjects, numMsgs);
                    }
                }
            }
        }
    }

private:
    void clearPassThruFunctions()
    {
        qDebug() << QThread::currentThreadId() << "T:clearPassThruFunctions_p";
        m_passThruOpen = 0;
        m_passThruClose = 0;
        m_passThruConnect = 0;
        m_passThruDisconnect = 0;
        m_passThruReadMsgs = 0;
        m_passThruWriteMsgs = 0;
        m_passThruStartPeriodicMsg = 0;
        m_passThruStopPeriodicMsg = 0;
        m_passThruStartMsgFilter = 0;
        m_passThruStopMsgFilter = 0;
        m_passThruReadVersion = 0;
        m_passThruGetLastError = 0;
    }

    QString passThruError() const
    {
        if (m_passThruGetLastError == 0)
        {
            return QString("PassThruGetLastError, function has not been resolved");
        }

        char buf[80];
        long retval = m_passThruGetLastError(buf);
        switch (retval)
        {
        case STATUS_NOERROR:
            return QString(buf);
        case ERR_NULL_PARAMETER:
            return QString("PassThruGetLastError, NULL pointer supplied where a valid pointer is required");
        default:
            return QString("PassThruGetLastError, unknown error or software bug");
        }
    }

private:
    unsigned long m_deviceId;
    bool m_isOpen;
    QTimer m_timer;
    QLibrary m_library;
    QList<unsigned long> m_channelIds;
};


J2534Device::J2534Device(QObject *parent) :
    QObject(parent)
{
    qDebug() << QThread::currentThreadId() << "M:J2534Device";

    m_libraryLoaded = false;
    m_isOpen = false;
    m_thread = new J2534Thread(0);
    Q_CHECK_PTR(m_thread);
    m_thread->moveToThread(m_thread); // This must be done before start

    QObject::connect(m_thread, SIGNAL(error(QString)), this, SLOT(onError(QString)), Qt::QueuedConnection);
    QObject::connect(m_thread, SIGNAL(channelData(ulong,void*,int)), this, SLOT(onChannelData(ulong,void*,int)));

    m_thread->start();
}

J2534Device::~J2534Device()
{
    this->close();
    QMetaObject::invokeMethod(m_thread, "unloadLibrary", Qt::BlockingQueuedConnection);
    QMetaObject::invokeMethod(m_thread, "deleteLater", Qt::QueuedConnection);
//    m_thread->terminate();
//    qDebug() << "del";
//    QMetaObject::invokeMethod(m_thread, "deleteLater", Qt::QueuedConnection);
//    qDebug() << "done";
//    QObject::deleteLater()
}

QString J2534Device::errorString() const
{
    return m_errorString;
}

bool J2534Device::isOpen() const
{
    return m_isOpen;
}

void J2534Device::onError(QString error)
{
    qDebug() << QThread::currentThreadId() << "M:onError" << error;
    emit deviceError(error);
}

void J2534Device::onChannelData(unsigned long channelId, void *dataObjects, int objectCount)
{
    PASSTHRU_MSG *msg = (PASSTHRU_MSG*)dataObjects;

    if (m_channels.contains(channelId))
    {
        m_channels.value(channelId)->addMessages(dataObjects, objectCount);
        // Channel is responsible for deleting the data
    }
    else
    {
        qCritical("onChannelData missing channel %lu", channelId);
        delete msg;
    }
}

bool J2534Device::setLibrary(QString functionLibraryPath)
{
    qDebug() << QThread::currentThreadId() << "M:setLibrary" << functionLibraryPath;

    // Close first if open
    if (m_isOpen)
    {
        close();
    }

    QString errorString;
    QMetaObject::invokeMethod(m_thread, "loadLibrary", Qt::BlockingQueuedConnection,
                              Q_RETURN_ARG(QString, errorString),
                              Q_ARG(QString, functionLibraryPath));

    if (errorString.size() != 0)
    {
        m_isOpen = false;
        m_libraryLoaded = false;
        m_errorString = errorString;
    }
    else
    {
        m_libraryLoaded = true;
    }

    return m_libraryLoaded;
}

bool J2534Device::open()
{
    qDebug() << QThread::currentThreadId() << "M:open";

    if (m_isOpen)
    {
        // Already open, so success
    }
    else if (m_libraryLoaded)
    {
        QString errorString;
        QMetaObject::invokeMethod(m_thread, "open", Qt::BlockingQueuedConnection,
                                  Q_RETURN_ARG(QString, errorString));
        if (errorString.size() != 0)
        {
            m_isOpen = false;
            m_errorString = errorString;
        }
        else
        {
            m_isOpen = true;
        }
    }
    else
    {
        m_errorString = "No function library has been set";
        m_isOpen = false;
    }

    return m_isOpen;
}

void J2534Device::close()
{
    qDebug() << QThread::currentThreadId() << "M:close";

    if (m_isOpen)
    {
        QString errorString;
        QMetaObject::invokeMethod(m_thread, "close", Qt::BlockingQueuedConnection,
                                  Q_RETURN_ARG(QString, errorString));
        if (errorString.size() != 0)
        {
            qCritical("J2534Device::close() failed %s\n", errorString.toLocal8Bit().constData());
        }

        // Notify all channels that we are closed
        foreach (int channelId, m_channels.keys())
        {
            J2534Channel *channel = m_channels.value(channelId);
            channel->setDeviceClosed();
        }
        m_channels.clear();
    }

    m_isOpen = false;
}

bool J2534Device::channelOpen(J2534Channel *channel, unsigned long protocolId, unsigned long baudrate)
{
    qDebug() << QThread::currentThreadId() << "M:createChannel";

    if (m_isOpen == false)
    {
        channel->setErrorString("Device is not open");
        return false;
    }

    unsigned long flags = CAN_29BIT_ID; // Use extended CAN IDs only
    unsigned long channelId;

    QString errorString;
    QMetaObject::invokeMethod(m_thread, "connectChannel", Qt::BlockingQueuedConnection,
                              Q_RETURN_ARG(QString, errorString),
                              Q_ARG(unsigned long *, &channelId),
                              Q_ARG(unsigned long, protocolId),
                              Q_ARG(unsigned long, flags),
                              Q_ARG(unsigned long, baudrate));

    if (errorString.size() != 0)
    {
        // Failed to connect channel
        channel->setErrorString(errorString);
        return false;
    }
    else
    {
        channel->setChannelId(channelId);
        m_channels.insert(channelId, channel);
        return true;
    }
}

void J2534Device::channelClose(J2534Channel *channel)
{
    qDebug() << QThread::currentThreadId() << "M:channelClose";
    unsigned long channelId = channel->channelId();
    if (m_channels.contains(channelId) && m_channels.value(channelId) == channel)
    {
        // Channel is part of the current connection, close it
        m_channels.remove(channelId);

        // TODO: Call disconnect on channelId
        QString errorString;
        QMetaObject::invokeMethod(m_thread, "disconnectChannel", Qt::BlockingQueuedConnection,
                                  Q_RETURN_ARG(QString, errorString),
                                  Q_ARG(unsigned long, channelId));
    }
    else
    {
        // Channel is no longer part of the current connection
    }
}

bool J2534Device::channelPassFilter(J2534Channel *channel, quint32 maskId, quint32 patternId, unsigned long *filterId)
{
    if (m_isOpen == false)
    {
        channel->setErrorString("Device is not open");
        return false;
    }

    unsigned long channelId = channel->channelId();
    if (m_channels.contains(channelId) && m_channels.value(channelId) == channel)
    {
        // Channel is part of the current connection
        unsigned long filterType = PASS_FILTER;
        PASSTHRU_MSG maskMsg;
        PASSTHRU_MSG patternMsg;
        memset(&maskMsg, 0, sizeof(PASSTHRU_MSG));
        memset(&patternMsg, 0, sizeof(PASSTHRU_MSG));

        maskMsg.ProtocolID = PROTOCOL_CAN;
        maskMsg.TxFlags = CAN_29BIT_ID;
        maskMsg.Data[0] = maskId >> 24;
        maskMsg.Data[1] = maskId >> 16;
        maskMsg.Data[2] = maskId >> 8;
        maskMsg.Data[3] = maskId;
        patternMsg.ProtocolID = PROTOCOL_CAN;
        patternMsg.TxFlags = CAN_29BIT_ID;
        patternMsg.Data[0] = patternId >> 24;
        patternMsg.Data[1] = patternId >> 16;
        patternMsg.Data[2] = patternId >> 8;
        patternMsg.Data[3] = patternId;

        QString errorString;
        QMetaObject::invokeMethod(m_thread, "channelFilterSetup", Qt::BlockingQueuedConnection,
                                  Q_RETURN_ARG(QString, errorString),
                                  Q_ARG(unsigned long, channelId),
                                  Q_ARG(unsigned long, filterType),
                                  Q_ARG(PASSTHRU_MSG*, &maskMsg),
                                  Q_ARG(PASSTHRU_MSG*, &patternMsg),
                                  Q_ARG(PASSTHRU_MSG*, 0),
                                  Q_ARG(unsigned long*, filterId));

        if (errorString.size() != 0)
        {
            channel->setErrorString(errorString);
            return false;
        }
        return true;
    }

    channel->setErrorString("Channel is not connected");
    return false;
}

QStringList J2534Device::devices()
{
    QSettings settings(l_passthruPath, QSettings::NativeFormat);
    return settings.childGroups();
}

QString J2534Device::deviceName(QString device)
{
    QSettings settings(l_passthruPath + "\\" + device, QSettings::NativeFormat);
    return settings.value("Name").toString();
}

QString J2534Device::deviceVendor(QString device)
{
    QSettings settings(l_passthruPath + "\\" + device, QSettings::NativeFormat);
    return settings.value("Vendor").toString();
}

QString J2534Device::deviceConfigApplication(QString device)
{
    QSettings settings(l_passthruPath + "\\" + device, QSettings::NativeFormat);
    return settings.value("ConfigApplication").toString();
}

QString J2534Device::deviceFunctionLibrary(QString device)
{
    QSettings settings(l_passthruPath + "\\" + device, QSettings::NativeFormat);
    return settings.value("FunctionLibrary").toString();
}

bool J2534Device::deviceSupportsCan(QString device)
{
    QSettings settings(l_passthruPath + "\\" + device, QSettings::NativeFormat);
    return settings.value("CAN", false).toBool();
}

bool J2534Device::deviceSupportsISO15765(QString device)
{
    QSettings settings(l_passthruPath + "\\" + device, QSettings::NativeFormat);
    return settings.value("ISO15765", false).toBool();
}

// Enable MOC:ing of this file
#include "J2534Device.moc"
