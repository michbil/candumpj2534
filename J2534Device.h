#ifndef J2534DEVICE_H
#define J2534DEVICE_H

#include "windows.h"
#include <QObject>
#include <QLibrary>
#include <QList>
#include <QHash>
#include <QString>
#include <QStringList>

// The J2534 API access resides in its own thread. This thread
// will constantly consume CPU power in order to poll the read
// functions for each channel.
//
// The J2534 API requires that all API calls are made from within
// the same thread. [section 6.2]
//
// Even though there is a blocking read function, this cannot be
// used since we may want to write data based on user interaction.
//
// The CPU usage can be reduced by calling the blocking read with
// a timeout. The price for this is added latency on all writes and
// also all reads from other channels.
//
class J2534Thread;
class J2534Channel;

class J2534Device : public QObject
{
    Q_OBJECT
public:

public:
    explicit J2534Device(QObject *parent = 0);
    ~J2534Device();

    /**
     * \brief Set which J2534 function library DLL file to use
     *
     * \retval true Successfully loaded DLL and resolved all needed symbols
     * \retval false Failed to load DLL, check errorString() for details.
     */
    bool setLibrary(QString functionLibraryPath);

    /**
     * \brief Open J2534 device
     *
     * \retval true Success
     * \retval false Failed to open, check errorString() for details.
     */
    bool open();

    /**
     * \brief Close J2534 device
     */
    void close();

    /**
     * \brief Check if device is open.
     *
     * \retval true Device is open
     * \retval false Device is closed
     */
    bool isOpen() const;

    /**
     * \brief Get the last error message
     *
     * \returns Human-readable string describing the last error that occured.
     */
    QString errorString() const;

signals:
    void deviceError(QString);
    void isOpenChanged(bool);

// Interface for channel objects
protected:
    bool channelOpen(J2534Channel *channel, unsigned long protocolId, unsigned long baudrate);
    void channelClose(J2534Channel *channel);
    bool channelPassFilter(J2534Channel *channel, quint32 maskId, quint32 patternId, unsigned long *filterId);
    friend class J2534Channel;

private slots:
    void onError(QString error);
    void onChannelData(unsigned long channelId, void *dataObjects, int objectCount);

private:
    bool m_libraryLoaded;
    bool m_isOpen;
    QString m_errorString;
    J2534Thread *m_thread;
    QHash<int, J2534Channel*> m_channels;

public:
    // Methods for listing and quering devices
    static QStringList devices();
    static QString deviceName(QString device);
    static QString deviceVendor(QString device);
    static QString deviceConfigApplication(QString device);
    static QString deviceFunctionLibrary(QString device);
    static bool deviceSupportsCan(QString device);
    static bool deviceSupportsISO15765(QString device);
};

#endif // J2534DEVICE_H
