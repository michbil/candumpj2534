#include <QDebug>
#include "J2534Device.h"
#include "J2534Channel.h"

typedef struct {
    unsigned long ProtocolID;
    unsigned long RxStatus;
    unsigned long TxFlags;
    unsigned long Timestamp;
    unsigned long DataSize;
    unsigned long ExtraDataIndex;
    unsigned char Data[4128];
} PASSTHRU_MSG;

// Protocol types
#define PROTOCOL_CAN               0x05
#define PROTOCOL_ISO15765          0x06

class J2534Channel::Chunk
{
public:
    // NOTE: The "packets" buffer is allocated by the J2534 thread, the channel is responsible for deleting them.
    PASSTHRU_MSG *packets;
    quint32 count;
    quint32 index;
};

J2534Channel::J2534Channel(QObject *parent) : QObject(parent)
{
    m_isOpen = false;
    m_device = 0;
    m_availablePackets = 0;
}

J2534Channel::~J2534Channel()
{
    if (m_isOpen)
    {
        m_device->channelClose(this);
    }
}

bool J2534Channel::open(J2534Device *device, CanProtocol protocol, quint32 baudrate)
{
    if (m_device)
    {
        this->close();
    }

    m_device = device;
    unsigned long protocolId;

    switch (protocol)
    {
    case RawCAN:
        protocolId = PROTOCOL_CAN;
        break;
    case ISO15765:
        protocolId = PROTOCOL_ISO15765;
        break;
    }

    // errorString set by J2534Device on failure
    return m_device->channelOpen(this, protocolId, baudrate);
}

void J2534Channel::close()
{
    m_device->channelClose(this);
    m_device = 0;
}

bool J2534Channel::isOpen() const
{
    return m_isOpen;
}

quint32 J2534Channel::packetsAvailable() const
{
    return m_availablePackets;
}

bool J2534Channel::setAcceptanceFilter(quint32 maskId, quint32 patternId)
{
    unsigned long filterId;
    // TODO: Store filterId so that filter can later be removed
    return m_device->channelPassFilter(this, maskId, patternId, &filterId);
}

quint32 J2534Channel::packetSize() const
{
    if (m_chunks.size() > 0)
    {
        Chunk *chunk = m_chunks.first();
        if (chunk->packets[chunk->index].DataSize < 4)
        {
            return 0;
        }
        else
        {
            return chunk->packets[chunk->index].DataSize - 4;
        }
    }
    return 0;
}

quint32 J2534Channel::readPacket(quint32* canId, char* data, quint32 maxSize)
{
    if (m_chunks.size() == 0)
    {
        // No packets available
        return 0;
    }

    Chunk *chunk = m_chunks.first();

    PASSTHRU_MSG *msg = &chunk->packets[chunk->index];

    quint32 size = 0;
    if (msg->DataSize >= 4)
    {
        quint32 id0 = msg->Data[0];
        quint32 id1 = msg->Data[1];
        quint32 id2 = msg->Data[2];
        quint32 id3 = msg->Data[3];
        *canId = (id0 << 24) |
                (id1 << 16) |
                (id2 << 8) |
                id3;
        size = msg->DataSize - 4;
        if (size > maxSize)
        {
            size = maxSize;
        }
        memcpy(data, &msg->Data[4], size);
    }

    m_availablePackets--;
    chunk->index++;
    if (chunk->index == chunk->count)
    {
        // No more data in this chunk
        m_chunks.removeFirst();
        free(chunk->packets);
        delete chunk;
    }

    return size;
}

void J2534Channel::writePacket(quint32 canId, const char *data, quint32 size)
{
}

QByteArray J2534Channel::readData()
{
    quint32 size = this->packetSize();
    if (size == 0)
    {
        return QByteArray();
    }
    else
    {
        QByteArray payload;
        payload.resize(size);
        quint32 canId;
        readPacket(&canId, payload.data(), size);
        return payload;
    }
}

void J2534Channel::writeData(QByteArray data)
{
}

QString J2534Channel::errorString() const
{
    return m_errorString;
}

// Interface functions for J2534 device

void J2534Channel::setErrorString(QString error)
{
    m_errorString = error;
}

void J2534Channel::setChannelId(unsigned long id)
{
    m_channelId = id;
}

unsigned long J2534Channel::channelId()
{
    return m_channelId;
}

void J2534Channel::setDeviceClosed()
{
    m_isOpen = false;
}

void J2534Channel::addMessages(void *dataObjects, int objectCount)
{
    Chunk *chunk = new Chunk();
    Q_CHECK_PTR(chunk);
    chunk->packets = (PASSTHRU_MSG*)dataObjects;
    chunk->count = objectCount;
    chunk->index = 0;
    m_chunks.append(chunk);
    m_availablePackets += chunk->count;
    emit readyRead();
}

