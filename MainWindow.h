#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "J2534Device.h"
#include "J2534Channel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void parseMessage(quint32 id, const char *data, int size);
    
private slots:
    void onDeviceError(QString error);
    void isOpenChanged(bool isOpen);
    void onNewData();
    void on_pushButtonConfigure_clicked();
    void on_pushButtonLoadDll_clicked();
    void on_pushButtonUnloadDll_clicked();
    void on_pushButtonOpen_clicked();
    void on_pushButtonClose_clicked();
    void on_pushButtonConnect_clicked();
    void on_pushButtonDisconnect_clicked();

private:
    Ui::MainWindow *m_gui;
    J2534Device m_j2534;
    J2534Channel m_channel;
};

#endif // MAINWINDOW_H
