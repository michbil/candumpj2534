#-------------------------------------------------
#
# Project created by QtCreator 2012-07-10T11:53:47
#
#-------------------------------------------------

QT       += core gui

TARGET = CanDumpJ2534
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    J2534Device.cpp \
    J2534Channel.cpp

HEADERS  += MainWindow.h \
    J2534Device.h \
    J2534Channel.h

FORMS    += MainWindow.ui

OTHER_FILES += \
    kvaser.txt
